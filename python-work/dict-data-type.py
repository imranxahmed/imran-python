#!/test/anaconda2/bin/python2
 
"""
 Dictionaries are unordered.

# number 1...To create a dict key:value combination 
d = {"favorite cat": None, "favorite spam": "all"}

# number 2
d = dict(one = 1, two=2,cat = 'dog') ; print d

# number 3 ... just start filling in items/keys
d = {}  # empty dictionary
d['cat'] = 'dog'
d['one'] = 1
d['two'] = 2

# number 4... start with a list of tuples
mylist = [("cat","dog"), ("one",1),("two",2)]
print dict(mylist)
