#!/usr/bin/python27

airports = {"DCA": "Washington, D.C.", "IAD": "Dulles", "LHR": "London-Heathrow", \
            "SVO": "Moscow", "CDA": "Chicago-Midway", "SBA": "Santa Barbara", "LAX": "Los Angeles",\
            "JFK": "New York City", "MIA": "Miami", "AUM": "Austin, Minnesota"}
            
# airline, number, heading to, gate, time (decimal hours) 
flights = [("Southwest",145,"DCA",1,6.00),("United",31,"IAD",1,7.1),("United",302,"LHR",5,6.5),\
           ("Aeroflot",34,"SVO",5,9.00),("Southwest",146,"CDA",1,9.60), ("United",46,"LAX",5,6.5),\
           ("Southwest",23,"SBA",6,12.5),("United",2,"LAX",10,12.5),("Southwest",59,"LAX",11,14.5),\
           ("American", 1,"JFK",12,11.3),("USAirways", 8,"MIA",20,13.1),("United",2032,"MIA",21,15.1),\
           ("SpamAir",1,"AUM",42,14.4)]

# Sort based on Airline
print format("Flight","16s"),format("Destination","<15s"),format("Gate",">10s"),format("Time",">10s")
print "-"*55
new = sorted(flights, key=lambda fli: fli[0])
for fli in new:
	print format(fli[0],"<10s"),format(fli[1],"<5d"),format(airports[fli[2]],"20s"),\
	format(fli[3],">5d"),format(fli[4],">10.1f")	

# Sort based on Arrival Times
print "\n\n\n",format("Flight","16s"),format("Destination","<15s"),format("Gate",">10s"),format("Time",">10s")
print "-"*55
new = sorted(flights, key=lambda fli: fli[4])
for fli in new:
	print format(fli[0],"<10s"),format(fli[1],"<5d"),format(airports[fli[2]],"20s"),\
	format(fli[3],">5d"),format(fli[4],">10.1f")	
