#!/usr/bin/python27

"""
	Purpose: To write some functions and explain concepts like modules,
                 and command-line programming.
	Input: Command-line args would be parsed as path to directory/file and
               display contents of a directory.
	Output:  To list files in the current path or under the given directory.
"""

import os
import sys

def getinfo(path=".",show_version=True):

	"""
	Purpose: Use os and sys modules.
	Input: path ( default = "." ), the directory u want to list
	"""

	if show_version:
		print "-" * 40
		print "You r using Python version ", #The comma would stop from executing linefeed or newline
		print sys.version
		print "-" * 40
	print "Files in the directory " + str(os.path.abspath(path)) + ":"
	for f in os.listdir(path): print "  " + f
	print "*" * 40

if __name__ == "__main__":

	"""
	Executed only if run from the command-line.
	Input: Call with $0 (script name).py <dirname> <dirname> ...
	If no dirname is given then list the files in the current path
	"""
	if len(sys.argv) ==1:
		getinfo(".",show_version=True)
	else:
		print "\n",sys.argv[:-1],"\n\n"
		for i,dir in enumerate(sys.argv[1:]):
			if os.path.isdir(dir):
			#if we have a directory then show it
			#Only show version info, if it's first time by (i==0) Test
				getinfo(dir,show_version=(i==0))
			else:
				print "Directory: " + dir + " does not exist." 
