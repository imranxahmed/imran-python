#!/usr/bin/python27

"""
	Purpose: use datetime module to calculate Your age
	Input: two vars a birth date and today's date as datetime object
	Output: difference of two dates results in datetime.timedelta() object
		which can be used to print days, age and date in 1000 days.

"""

import datetime

birth = datetime.datetime(1968,10,28)
today = datetime.datetime.now()

age = today - birth
print '>' * 10,"I have lived for: ",age.days," days."
print '>' * 10,"I have lived for: ",age.days * 24," Hours."
print '>' * 10,"The new date in 1000 days: ",today + datetime.timedelta(days=1000)
