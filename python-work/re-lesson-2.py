#!/test/anaconda/bin/python

"""
    This exmaple is of pulling in re module and working
    with strings.

    .(dot) -Matches any character except Newline.
    \(backslash) -Allows certain characters to translated literally.
    "r" - Prepending the patern with "r" makes sure that we interpret literal match.
    "\S" Match non-whitespace character. 
    "\S+" At least ONE non-whitespace character.

"""
import re

hand = open('test' , 'r')
for eachline in hand:
    eachline = eachline.rstrip()
    num = re.findall('\S+@\S+', eachline)
    print type(num),len(num),num
    num = re.findall('^From (\S+@\S+)', eachline)
    print type(num),len(num),"Second match",num
