#!/test/anaconda2/bin/python2
"""
 To get the lenght of list, we use len() function with range() """

a = ['Mary', 'had', 'a', 'little', 'lamb']
for i in range(len(a)):
    print i, a[i]
