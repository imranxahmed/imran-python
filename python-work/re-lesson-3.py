#!/test/anaconda/bin/python

"""
    This exmaple is of pulling in re module and working
    with strings.

    "\S" Match non-whitespace character. 
    "\S+" At least ONE non-whitespace character.
    Slicing string by finding length from certain mark to certain mark

"""
import re

hand = open('test' , 'r')
for eachline in hand:
    eachline = eachline.rstrip()
    mark1 = eachline.find('@')
    print type(mark1),mark1
    mark2 = eachline.find(' ', mark1)
    print type(mark2),mark2
    host = eachline[mark1+1 : mark2]
    print "Hostname:",host
