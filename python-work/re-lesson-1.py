#!/test/anaconda/bin/python

"""
    This exmaple is of pulling in re module and working
    with strings.

    .(dot) -Matches any character except Newline.
    \(backslash) -Allows certain characters to translated literally.
    "r" - Prepending the patern with "r" makes sure that we interpret literal match.

"""
import re

hand = open('test' , 'r')
for eachline in hand:
    eachline = eachline.rstrip()
    num = re.findall('[0-9]+', eachline)
    print type(num),len(num),num
