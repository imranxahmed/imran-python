<!DOCTYPE html>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# githubog: http://ogp.me/ns/fb/githubog#">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Dictionary Access</title>

  <meta content="authenticity_token" name="csrf-param" />
<meta content="sdWeKgd1l6VWC6MFtowR6OfKlBQcOPsSYW3eNWfRxbw=" name="csrf-token" />
  <meta name="viewport" content="width=960">


  <link type="text/plain" rel="author" href="https://github.com/humans.txt" />
  <meta content="gist" name="octolytics-app-id" /><meta content="collector.githubapp.com" name="octolytics-host" /><meta content="collector-cdn.github.com" name="octolytics-script-host" />

  <link href="/assets/application-4cf4f070c27ac7738aa88136815c0ab7.css" media="screen, print" rel="stylesheet" />
  <script src="/assets/application-3476deec114666268e470af85a57c060.js"></script>

      <meta name="robots" content="noindex, nofollow">

</head>

<body class=" ">

  <div id="wrapper">
    

    <div id="header" class="header header-logged-out">
      <div class="container" class="clearfix">
        <a class="header-logo-wordmark" href="https://gist.github.com/">
          <span class="octicon octicon-logo-github"></span>
          <span class="octicon-logo octicon-logo-gist"></span>
        </a>

        <div class="header-actions">
          <a class="button primary" href="https://github.com/signup?return_to=gist">Sign up for a GitHub Account</a>
          <a class="button" href="https://gist.github.com/login?return_to=/dirn/9101f01370a8fd20efe4" data-skip-pjax>Sign in</a>
        </div>
      </div>
    </div>

    <div class="site-content" id="js-pjax-container" data-pjax-container>
      <div class="vis-secret site-container js-site-container" data-url="/dirn/9101f01370a8fd20efe4">
  
  <meta content="false" name="octolytics-dimension-public" /><meta content="8642502" name="octolytics-dimension-gist_id" /><meta content="9101f01370a8fd20efe4" name="octolytics-dimension-gist_name" /><meta content="false" name="octolytics-dimension-anonymous" /><meta content="168109" name="octolytics-dimension-owner_id" /><meta content="dirn" name="octolytics-dimension-owner_login" /><meta content="false" name="octolytics-dimension-forked" />

<div class="pagehead repohead">
  <div class="container">
    <div class="title-actions-bar">
      <ul class="pagehead-actions">


      </ul>
      <h1 itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="entry-title secret">
        <span class="repo-label"><span>secret</span></span>
        <span class="mega-octicon octicon-gist-secret tooltipped downandright" title="Secret gists are hidden from search engines but visible to anyone you give the URL."></span>
        <div class="meta">
          <div class="gist-author">
            <img src="https://1.gravatar.com/avatar/9562cbc3556ac68f081dfda387a9f4ab?d=https%3A%2F%2Fidenticons.github.com%2F148215400e895fe40af9e6d7d5999aa3.png&s=140" width="26" height="26">
            <span class="author vcard">
                <span itemprop="title"><a href="/dirn">dirn</a></span>
            </span> /
            <strong><a href="/dirn/9101f01370a8fd20efe4" class="js-current-repository">dictionary.rst</a></strong>
          </div>
          <div class="gist-timestamp">
              <span class="datetime">Last active <time class="js-relative-date" title="2014-01-27T02:41:35Z" datetime="2014-01-27T02:41:35Z">2014-01-27</time></span>
          </div>
      </h1>
    </div>

  </div>
</div>



  <div class="gist-description container">
    <p><div>Dictionary Access</div></p>
  </div>
<div class="gist container js-gist-container" data-version="53b51eb218087acb363a7bf92c054f41748e7953">
    <div class="root-pane">
  <div class="menu-container">
    <ul class="menu gisttabs">
      <li>
        <a href="/dirn/9101f01370a8fd20efe4" class="selected">
          Gist Detail
        </a>
      </li>

        <li class="revision-count">
          <a href="/dirn/9101f01370a8fd20efe4/revisions" >
            Revisions
            <span class="counter">5</span>
          </a>
        </li>


    </ul>
  </div>

  <ul class="export-references">
    <li>
      <a href="/dirn/9101f01370a8fd20efe4/download" class="minibutton" data-skip-pjax="true" rel="nofollow"><span class="octicon octicon-cloud-download"></span>Download Gist</a>
    </li>
    <li>
      <label for="url-field"><strong>Clone</strong> this gist</label>
      <input type="text" readonly spellcheck="false" class="url-field js-url-field js-copy-toggle" name="url-field" value="https://gist.github.com/9101f01370a8fd20efe4.git" data-copy-toggle-display="/dirn/9101f01370a8fd20efe4">
    </li>
    <li>
      <label for="embed-field"><strong>Embed</strong> this gist</label>
      <input type="text" readonly spellcheck="false" class="url-field js-url-field" name="embed-field" value="&lt;script src=&quot;https://gist.github.com/dirn/9101f01370a8fd20efe4.js&quot;&gt;&lt;/script&gt;">
    </li>
    <li>
      <label for="link-field"><strong>Link to</strong> this gist</label>
      <input type="text" readonly spellcheck="false" class="url-field js-url-field" name="link-field" value="https://gist.github.com/dirn/9101f01370a8fd20efe4">
    </li>
  </ul>
</div>


  <div class="column files">
        <div id="file-dictionary-rst" class="bubble">
          <div class="file-box ">
            <div class="meta">
              <div class="file-info">
                <span class="file-type-icon"><span class="octicon octicon-gist"></span></span>
                <strong class="file-name js-selectable-text">dictionary.rst</strong>
              </div>
              <div class="file-actions">
                <span class="file-language">reStructuredText</span>
                <ul class="button-group">
                  <li><a title="Permalink" href="#file-dictionary-rst" class="file-actions-button tooltipped downwards permalink"><span class="octicon octicon-link"></span></a></li>
                  <li><a title="View Raw" href="/dirn/9101f01370a8fd20efe4/raw/ff9e55683604ad2b0c2ccfcd4020283c51ada52c/dictionary.rst" data-skip-pjax class="file-actions-button tooltipped downwards raw-url"><span class="octicon octicon-code"></span></a></li>
                </ul>
              </div>
            </div>
            <div class="suppressed">
              <a class="js-show-suppressed-file">File suppressed. Click to show.</a>
            </div>
            

    <div class="readme context-loader-container context-loader-overlay" id="readme">
      <article class="markdown-body js-file "
          data-task-list-update-url="/dirn/9101f01370a8fd20efe4/file/dictionary.rst">
        <div>
<p>Let's examine the <tt>phone_numbers</tt> dictionary:</p>
<pre>
&gt;&gt;&gt; phone_numbers = {
    'family': [('mom','642-2322'), ('dad','534-2311')],
    'friends': [('Billy','652-2212')],
}
</pre>
<p>The values in a dictionary are accessible through their keys. The <tt>phone_numbers</tt> dictionary has the keys <tt>'family'</tt> and <tt>'friends'</tt>:</p>
<pre>
&gt;&gt;&gt; list(phone_numbers.keys())
['family', 'friends']
</pre>
<p>It contains the values <tt>[('mom', '642-2322'), ('dad', '534--2311')]</tt> and <tt>[('Billy', '652-2212')]</tt>:</p>
<pre>
&gt;&gt;&gt; list(phone_numbers.values())
[[('mom', '642-2322'), ('dad', '534-2311')], [('Billy', '652-2212')]]
</pre>
<p>If you examine each value, you will see that it is a list:</p>
<pre>
&gt;&gt;&gt; type(phone_numbers['family'])
&lt;class 'list'&gt;
</pre>
<p>This means that when you iterate over <tt>phone_numbers[group_type]</tt>, <tt>info</tt> is set to each value within in the list. If you examine, it, you will see that it is a tuple:</p>
<pre>
&gt;&gt;&gt; type(phone_numbers['family'][0])
&lt;class 'tuple'&gt;
</pre>
<p>Because <tt>info</tt> is a tuple, there is no support for key access. Its values can only be accessed by index. Therefore <tt>'mom'</tt> and <tt>'Billy'</tt> cannot be used to retrieve the numbers associated with them.</p>
<p>Knowing all of this, your sample code can perhaps be simplified a little:</p>
<pre>
for group_type in phone_numbers:  # This is similar to using `for group_type in phone_numbers.keys()`.
    print('Group ' + group_type + ':')
    for info in phone_numbers[group_type]:
        print ' ', info[0], info[1]
</pre>
<p>This can further be simplified by using the <tt>items()</tt> method:</p>
<pre>
for group_type, entries in phone_numbers.items():
    print('Group ' + group_type + ':')
    for name, number in entries:  # This only works if all items in entries contain two values.
        print ' ', name, number
</pre>
<p>(Note that if you're using Python 2.x, you'll want to substitute <tt>iteritems</tt>, <tt>iterkeys</tt>, and <tt>itervalues</tt> for <tt>items</tt>, <tt>keys</tt>, and <tt>values</tt>, respectively.)</p>
<p>If you were to change the the values associated with the keys to be dictionaries rather than lists of two-value tuples, you'd be able to access the numbers through their keys, although, as you'll see, Python does not offer any special syntax for offering values within nested dictionaries:</p>
<pre>
&gt;&gt;&gt; phone_numbers = {
    'family': {'mom': '642-2322', 'dad': '534-2311'},
    'friends': {'Billy': '652-2212'},
}
&gt;&gt;&gt; list(phone_numbers.keys())
['family', 'friends']
&gt;&gt;&gt; list(phone_numbers['family'].keys())
['dad', 'mom']
&gt;&gt;&gt; phone_numbers['family']['mom']
'642-2322'
</pre>
<p>The values of the dictionary inside <tt>phone_numbers['family']</tt> is accessible the same way the values of <tt>phone_numbers</tt> are:</p>
<pre>
&gt;&gt;&gt; phone_numbers['family']['mom']
'642-2322'
</pre>
<p>Here's another way to view this conceptually:</p>
<pre>
&gt;&gt;&gt; family = phone_numbers['family']
&gt;&gt;&gt; family['mom']
'642-2322'
</pre>
</div>
      </article>
    </div>



          </div>
        </div>
    <div id="comments" class="new-comments">
      
    </div>
      <p class="uncommentable"><span class="octicon octicon-alert"></span> Please <a href="/login?return_to=/dirn/9101f01370a8fd20efe4" rel="nofollow">sign in</a> to comment on this gist.</p>
  </div>
</div>

  <div class="context-overlay"></div>
</div>

    </div>
    <div class="slow-loading-overlay"></div>
  </div>

  <div id="ajax-error-message" class="flash flash-error">
    <div class="container">
      <span class="octicon octicon-alert"></span>
      Something went wrong with that request. Please try again.
      <a href="#" class="octicon octicon-remove-close ajax-error-dismiss"></a>
    </div>
  </div>

  <footer>
    <div id="footer">
  <div class="container clearfix">

    <!-- Served fresh by github-fe103-cp1-prd.iad.github.net -->
    <p class="right">&copy; 2014 GitHub Inc. All rights reserved.</p>
    <a class="left" href="/">
      <span class="mega-octicon octicon-mark-github"></span>
    </a>
    <ul id="legal">
      <li><a href="https://github.com/blog">The GitHub Blog</a></li>
      <li><a href="mailto:support@github.com">Support</a></li>
      <li><a href="https://github.com/contact">Contact</a></li>
    </ul>

  </div><!-- /.container -->
</div><!-- /.#footer -->

  </footer>

</body>
</html>
