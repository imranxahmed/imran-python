#!/usr/bin/python27

"""
	Purpose: To test whether a year is leap year or not
"""

import sys

# A leap year if div by 4 but NOT by 100 OR div by 400
#yr = input("What Year U want to Chk:  ")

if len(sys.argv) == 2:
	yr = int(sys.argv[1])
	print  "isThisYearLeap ",yr,":",( yr % 4 == 0 or yr % 400 == 0) and ( yr % 100 != 0) 
else:
	print "Not Enough Args: ",len(sys.argv)
