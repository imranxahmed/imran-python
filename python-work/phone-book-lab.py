#!/usr/bin/python27

""" A workbook exmaple of dictionary
	Purpose: To show how to iterate over various dictionary items.

	The program offers various ways to access dictionary items and 
	introduces items() & keys() functions on dictionaries.

	Output: prints 2 elements of each category of dictionary """
 
phone_numbers = {'family': [('mom','642-2322'),('dad','534-2311')],\
                     'friends': [('Billy','652-2212')]}

#Following is the code to iterate over this dictionary:

for group_type in ['friends','family']:
        print "Group " + group_type + ":"
        for info in phone_numbers[group_type]:
             print " ",info[0], info[1]

#.keys() and .values(): are called methods on dictionaries
# phone_numbers.key() & phone_numbers.values()
#groups = phone_numbers.keys()
#groups.sort()
#for group_type in groups:
#        print "Group " + group_type + ":"
#        for info in phone_numbers[group_type]:
#             print " ",info[0], info[1]

#.iteritems() is a handy method, returning key,value pairs with each iteration
#for group_type, vals in phone_numbers.iteritems():
#        print "Group " + group_type + ":"
#        for info in vals:
#             print " ",info[0], info[1]
#
