#!/usr/bin/python27

"""
	Function to Day 1 section III PYTHON bootcamp solution;
	Input: if 1 argument, then print new date from now
	Input: if 3 args <yr> <mo> <day>, list days since that day
"""

import datetime
import sys

def days_from_now(Ndays):
	"""Publish date from now """
	now = datetime.datetime.now()
	future = now + datetime.timedelta(Ndays)
	#print "In ", Ndays," date will be ...",future
	""" Since future is datetime object, we have to convert to str to display properly"""
	""" We only called yr/mo/day by calling object date() of datetime class """
	return "In " + str(Ndays) + " days, date is: " + str(future.date())

def since_days(yr,mo,day):
	""" Print elapsed days since given date """
	since = datetime.datetime(yr,mo,day)
	now = datetime.datetime.now()
	diff = now - since
	return "It has been " + str(diff.days) + " days since " + str(since.date())

if __name__ == "__main__":
	
	if len(sys.argv) == 2 :
		""" Only 1 arg given """
		result =  days_from_now(int(sys.argv[1]))
	elif len(sys.argv) == 4 :
		""" a date yr mo day is given """
		result = since_days(int(sys.argv[1]),int(sys.argv[2]),int(sys.argv[3]))
	else :
		result =  " Error: Not Enough args given ",repr(sys.argv[:])
		print '#'*20,"\n\n\t","Usage: ",sys.argv[0]," <Num of Days> "
		print "\t",sys.argv[0]," <year> <month> <day> \n\n",'#'*20,"\n\n"


print result
