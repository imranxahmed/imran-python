#!/usr/bin/python27

import math

"""
        The formula for distance 
	is sqrt((x2 - x1)**2 + (y2 - y1)**2)
"""
xval1,yval1 = input("Enter x1 & y1 for 1st Point: ")
xval2,yval2 = input("Enter x2 & y2 for 2nd Point: ")

##pdist = (((xval2 - xval1 )**2) + ((yval2 - yval1)**2))**0.5
pdist = (((xval1 - xval2 )**2) + ((yval1 - yval2)**2))**0.5
sqrtDist = math.sqrt(((xval1 - xval2 )**2) + ((yval1 - yval2)**2))

print "The formula distance between ",xval1,yval1,"&a",xval2,yval2,"is: ",pdist
print "The sqrt distance between ",xval1,yval1,"&a",xval2,yval2,"is: ",sqrtDist
