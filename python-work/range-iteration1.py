#!/test/anaconda2/bin/python2
"""
   The range() function creates subset of any list, starting with index(0)
"""
x = range(4) ; print x
total = 0
for val in range(4):
        total += val
        print "By adding " + str(val) + \
              " the total is now " + str(total)
