#!/usr/bin/python27

from fractions import Fraction

"""
	Purpose: To convert temperature from F --> C or C --> F
	Input: Using input() enter integer; Single input expected for temperature either in Celcius or Feranheight
	Output:Converted Temperature
"""

# A program to convert from F to C & vice versa.
# C --> F [ C x 9/5 + 32 = F] --- F --> C [(F - 32) * 5/9 = C

print("Enter '1' (C --> F) | '2' (F -->C) ")
_input = input()

if _input == 1 : 
	print("Enter temperature in Celcius degrees ")
	celcius = input()
	#totFaren = Decimal.fma(celcius,1.8,32)
	totFaren = celcius * Fraction(9,5) + 32
	print "Currently it is: ", float(totFaren) , "F"
	#print "Currently it is: ", celcius * Fraction(9,5) + 32, "F"

elif _input == 2 :
	print("Enter temperature in Faren degrees ")
	faren = input()
	totcel = ((faren - 32)  * Fraction(5,9)) 
	print "Currently it is: ", float(totcel), "C"

