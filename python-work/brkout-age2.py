#!/usr/bin/python27

import datetime
import sys
"""
	Purpose: use datetime module to calculate Your age
	Input: Single variable used as timedelta interval,3 variables serve as birth date 
	Output: Print datetime.timedelta() object by adding interval to current date.
		Print days since given birth date if 3 variables given.

"""

today = datetime.datetime.now()

if len(sys.argv) == 2:
	# The number starts at 1 & Index starts with 0 
	period = sys.argv[1]
	print '>' * 10,"Date in ",period," days.",datetime.datetime.now() + datetime.timedelta(days=int(period))
	#newdate = datetime.datetime.now() + datetime.datetime(days=interval)
elif len(sys.argv) == 4:
	yr = int(sys.argv[1])
	mo = int(sys.argv[2])
	dy = int(sys.argv[3])
	birth = datetime.datetime(yr,mo,dy)
	daysince = today - birth
	print '>' * 10,"Days since then...",daysince.days 
else :
	print "Not enough arguments: " + repr(sys.argv[:])
