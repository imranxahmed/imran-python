#!/usr/bin/python26

# To compute how much change in quarters, dimes and nickles.


amount = input("Enter Amount, e.g 11.56: ")

totCents = int(amount * 100)
totDollars = totCents / 100


totCents = totCents % 100

totQuarters = totCents / 25
totCents = totCents % 25

totDimes = totCents / 10
totCents = totCents % 10

totNickles = totCents / 5
totCents = totCents % 5

totPennies = totCents

print"Amount of ",amount,"dollar has " \
	,"\n\t\t",totCents," Pennies" \
	,"\n\t\t",totDollars," US Dollars" \
	,"\n\t\t",totQuarters," Quarters"\
	,"\n\t\t",totDimes," Dimes" \
	,"\n\t\t",totNickles," Nickles"
