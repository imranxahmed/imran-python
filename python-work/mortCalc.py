#!/usr/bin/python27
"""
	Ver 1.0 8/2/2013 ident @ only calculated basic values, to be modified further
	Compute mortgage
"""

numYrs=12

loanAmt = input("Enter the amount of loan :")
intRate = input("What is the interest Rate :") / 1200 #Since its a %, we convert the rate into decimal
loanAge = input("How many years to pay off :")

monIntRate = intRate / 12
monPayment = loanAmt * monIntRate / ( 1 - 1 / ( 1 + monIntRate ) ** ( loanAge * 12 ))
totPayment = monPayment * loanAge * numYrs

print "Principal of", loanAmt, "at", intRate, "will be paid off in ", loanAge, "years. "
