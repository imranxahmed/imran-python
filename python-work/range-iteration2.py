#!/test/anaconda2/bin/python2

"""
range([start,] stop[, step]) . list of integers
"""

total = 0
for val in range(1,10,2):
    total += val
    print "By adding " + str(val) + \
          " the total is now " + str(total)
