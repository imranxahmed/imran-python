#!/usr/bin/python27

"""
	Function to calculate Epoh Unix time since Midnight Jan 1st 1970
	Input: 
	Input: 
"""

import time
import datetime
import sys

def epoh_time(yr,mo,dy):
	""" Print elapsed days since given date """
	since = datetime.datetime(yr,mo,day)
	now = datetime.datetime.now()
	diff = now - since
	return "It has been " + str(diff.days) + " days since " + str(since.date())

if __name__ == "__main__":
	
	if len(sys.argv) == 1 :
		""" No arg given """
		since = datetime.datetime(1970,1,1)
		now = datetime.datetime.now()
		diff = now - since
		print "\t\t Epoh Time: ",time.time()
		for idx in len(diff):
			print "\n",diff[idx]
			idx=--
	else :
		result =  " Error: Not Enough args given ",repr(sys.argv[:])
		print '#'*20,"\n\n\t","Usage: ",sys.argv[0]," <Num of Days> "
		print "\t",sys.argv[0]," <year> <month> <day> \n\n",'#'*20,"\n\n"


#print result
