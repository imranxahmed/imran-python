#!/test/anaconda/bin/python

"""
    regex_sum_324898.txt  read the file, extract numbers
    get sum of numbers

    "\S" Match non-whitespace character. 
    "\S+" At least ONE non-whitespace character.
    Slicing string by finding length from certain mark to certain mark

"""
import re

print "**** NOTE **** Do NOT strip EOL marker for this assignment"
acc = count = 0
hand = open('regex_sum_324898.txt' , 'r')
for line in hand:
    rem = re.findall('(\d+)\s', line)
    if len(rem) > 0:
       count = count + len(rem)
       for item in range(len(rem)):
           acc = acc  + int(rem[item])
print "Sum of ",count," items =",acc
