#!/usr/bin/python26

# This program computes Area of pentagon
# To calculate side (s) use formula s = 2r sin(pi/5)
# Import math module

import math

vertLength = input("Please enter length from center --> Vertex")
sideLen = (2 * vertLength ) *  (math.sin(math.pi/5))
pentArea = ((3 * math.sqrt(3)) / 2 ) * math.pow(sideLen,2)


print "\tThe length you entered was: \t", vertLength
print "\tThe area of a Pentagon is:  \t", pentArea
