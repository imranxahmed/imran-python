#!/test/anaconda/bin/python

"""
    This exmaple is of pulling in re module and working
    with strings.

    "\S" Match non-whitespace character. 
    "\S+" At least ONE non-whitespace character.
    Slicing string by finding length from certain mark to certain mark

"""
import re

hand = open('test1' , 'r')
for eachline in hand:
    eachline = eachline.rstrip()
    rem = re.findall('@([\s+]*)', eachline)
    print rem
    mark =  eachline.find('@')
    print mark
    end =  eachline.find(' ', mark)
    print end
    print eachline[mark+1 : end ]
    good = re.findall('^F.+:', eachline)
    print "Line matches:",type(good),good
