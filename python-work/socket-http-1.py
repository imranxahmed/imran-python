#!/test/anaconda/bin/python

"""
     First exmaple to connect to a site and pull some data
tsoc.send('GET http://www.py4inf.com/code/romeo.txt HTTP/1.0\n\n')
tsoc.send('GET http://www.bvminc.net HTTP/1.0\n\n')

"""

import socket

tsoc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tsoc.connect(('www.py4inf.com',80))

tsoc.send('GET http://data.pr4e.org/intro-short.txt HTTP/1.0\n\n')
while True:
   data = tsoc.recv(10240)
   if (len(data) < 1 ):
      print "Bad Data:\n",data
      break
   print len(data)
   print "\n******Rcvd******\n",data
   print "***END****\n"
tsoc.close()
