#!/usr/bin/python27

# A socket is end of point of a connection,just like files u can read & write 
# They r bi-directional, so U can Rd & Wte at the sametime
#A client does 3 things: connect / send / recv data
#A TCP client

import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('localhost', 8081))
s.send('Happy Hacking')
data = s.recv(1024)
s.close()
print 'Received:'
print data
