#!/usr/bin/python26

#To compute Taxes in US
import math
import sys

fStatus = input ("\tEnter Filing Status\n" + 
                "\t(0:Filing Single )" + 
                "(1:Married Filing jointly )"  + 
                "(2:Married Filing Separatly )" + 
                "(3:Head of the Household )")

tIncome = input("\tEnter Taxable Income :")
taxyear = input("\tPlease enter Tax Year :")
fedLibility = 0
singletenPerTax = 8350 * 0.10
singlefifPerTax = 33950 * 0.15
singletwentyFivTax = 82250 * 0.25
singletwentyEigTax = 171550 * 0.28
singlethirtyThrTax = 372950 * 0.33

marriedtenTax = 16700 * 0.10

if fStatus == 0:
	print "\tYou r Filing as Single  "
	if tIncome <= 8350:
		fedLibility = singletenPerTax
	elif tIncome <= 33950:
		deduc +=8350
		fedLibility = singletenPerTax + (tIncome - deduc ) * 0.15
	elif tIncome <= 82250:
		deduc = 33950 + 8350
		fedLibility = singletenPerTax + singlefifPerTax + (tIncome - deduc) * 0.25
	elif tIncome <= 171550:
		deduc = 82250 + 33950 + 8350
		fedLibility = singletenPerTax + singlefifPerTax + singletwentyFivTax \
		+ (tIncome - deduc ) * 0.28
	elif tIncome <= 372950:
		deduc = 171550 + 82250 + 33950 + 8350
		fedLibility = singletenPerTax + singlefifPerTax + singletwentyFivTax + singletwentyEigTax \
                + (tIncome - deduc ) * 0.33
	else:
		deduc = 372950 + 171550 + 82250 + 33950 + 8350
		fedLibility = singletenPerTax + singlefifPerTax + singletwentyFivTax + singletwentyEig + singlethirtyThrTax \
                + (tIncome - deduc ) * 0.35

elif fStatus == 1:
	print "\tYou r Filing as Married Filing Jointly "
	if tIncome <= 16700:
		fedLibility = marriedtenTax
elif fStatus == 2:
	print "\tYou r Married Filing Separately "
elif fStatus == 3:
	print "\tYou r Filing as Head of Household "
else:
	print "\tError: invalid selection "
	sys.exit()

print "Your Tax liability for Tax Year ", taxyear, "is ", format(fedLibility, ".3f")
